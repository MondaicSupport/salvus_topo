# Salvus*Topo*

Open-source topography preprocessing tools for global planetary data for use
with the Salvus software suite - you are of course free to use it for other
purposes as well.

Please also feel free to modify any of the files to be able to generate your
own topography file. This repository contains all the information you need to
get started.

## Installation

The major difficulty in the installation are the
[`SHTns`](https://bitbucket.org/nschaeff/shtns/src/default/) Python wrappers
but even that is manageable.

`FFTW3` is required, thus install it:

```
# On OSX
brew install fftw
# On Ubuntu/Debian
sudo apt install libfftw3-dev
```

Best create a separate `conda` environment for SalvusTopo:

```
conda create -n salvus_topo python=3.7
conda activate salvus_topo
conda install numpy ipython netCDF4
```

Now install the `SHTns` Python wrappers.

```
git clone https://bitbucket.org/nschaeff/shtns.git
cd shtns
# Make sure the correct Python is active
./configure --enable-python
make -j
python setup.py install
```

Last but not least install this package.

```
git clone https://gitlab.com/MondaicSupport/salvus_topo.git
cd salvus_topo
pip install -v -e .
```

## Data Sources

---

**DISCLAIMER**

SalvusTopo will download data from third party data sources and you are
responsible to ensure that you have the right to use it. Have a look at [this
file](https://gitlab.com/MondaicSupport/salvus_topo/blob/master/salvus_topo/remote_data.py) to inspect the actually downloaded URLs.

---

As of now, the recipes part of SalvusTopo, use three data sources(**Please
cite them if you end up using it**):

#### Earth Gravitational Model 2008 (EGM2008)

https://earth-info.nga.mil/GandG/wgs84/gravitymod/egm2008/

Specifically we use the Geoid undulations with respect to WGS84. In the
following we'll refer to this as `Geoid`

```
Pavlis, Nikolaos K., Simon A. Holmes, Steve C. Kenyon, and John K. Factor. 2012.
“The Development and Evaluation of the Earth Gravitational Model 2008 (EGM2008).”
Journal of Geophysical Research: Solid Earth 117 (4): 1–38.
https://doi.org/10.1029/2011JB008916.
```

#### Earth2014 global topography (relief) model

http://ddfe.curtin.edu.au/models/Earth2014

https://www.bgu.tum.de/iapg/forschung/topographie/earth2014/

Height in these is always relative to the mean sea level.

Specifically we use two pieces of data: The spherical harmonics coefficients
for the Earth surface (referred to as `Earth2014_SUR`) and the for the Topo
bedrock, bathymetry, and ice (referred to as `Earth2014_TBI`).

```
Hirt, C. and Rexer, M. (2015)
Earth2014: 1 arc-min shape, topography, bedrock and ice-sheet models - available as gridded data and degree-10,800 spherical harmonics,
International Journal of Applied Earth Observation and Geoinformation 39, 103-112,
https://doi.org/10.1016/j.jag.2015.03.001.
```

#### Crust 1.0

https://igppweb.ucsd.edu/~gabi/crust1.html

We use this to compute the position of the Moho boundary on Earth.

Specifically we use three pieces of data: The topography (referred to as
`CRUST1.0_TOPO`), the ocean (referred to as `CRUST1.0_OCEAN`), and the
crustal thickness (referred to as `CRUST1.0_CRUSTAL_THICKNESS`).

```
Laske, G., Masters., G., Ma, Z. and Pasyanos, M.,
Update on CRUST1.0 - A 1-degree Global Model of Earth's Crust,
Geophys. Res. Abstracts, 15, Abstract EGU2013-2658, 2013.
```

## Usage

Once installed it can generate combined models, which the Salvus suite can
use for various purposes.

#### Earth Topography

`Topography = Geoid + Earth2014_TBI`

Max support lmax: 10800

```shell
$ salvus-topo create topography-from-earth2014-and-egm2008 \
  --max-lmax=256 \
  --output-filename=topography_earth2014_egm2008.nc
```

#### Earth Moho Topography

`Moho Topography = Geoid + CRUST1.0_TOPO - CRUST_1.0_OCEAN - CRUST1.0_CRUSTAL_THICKNESS`

Max support lmax: 256

```shell
$ salvus-topo create moho-topography-from-crust-1-0-and-egm2008 \
  --max-lmax=256 \
  --output-filename=moho_topography_crust_1_0_egm2008.nc
```

#### Earth Bathymetry

`Bathymetry = Earth2014_SUR - Earth2014_TBI`

Max support lmax: 10800

```shell
$ salvus-topo create bathymetry-from-earth2014 \
  --max-lmax=256 \
  --output-filename=bathymetry_earth2014.nc
```
