"""
A class to read, write and filter geophysical models defined on the surface
of the sphere.

(c) Mondaic AG (info@mondaic.com), 2020
"""
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.image import NonUniformImage
import numpy as np
from scipy.interpolate import RectSphereBivariateSpline, RectBivariateSpline
from netCDF4 import Dataset
import xarray as xr
import pathlib

# import cf
from .sht import sht


CRUST_1_0_LAYER = {
    "topography": 0,
    "ocean": (0, 1),
    "crust": (1, -1),
    "sediment": (1, 5),
}

ELLIPSOIDS = {
    "GRS80": 1.0 / 298.257222101,
    "WGS84": 1.0 / 298.257223563,
    "MARS": 0.00589,
}


class SphereSurfModel(object):
    def __init__(self, data_space, phi=None, theta=None, grid="None"):
        self.data_space = data_space
        self.grid = grid

        if not grid == "regular" and theta is None:
            raise ValueError()

        if phi is None:
            self.phi = self.cell_centered_grid_phi(self.nphi)
        else:
            self.phi = phi.copy()

        if theta is None:
            self.theta = self.cell_centered_grid_theta(self.ntheta)
        else:
            self.theta = theta.copy()

    def __add__(self, other, rtol=1e-10):
        np.testing.assert_allclose(self.theta, other.theta, rtol=rtol)
        np.testing.assert_allclose(self.phi, other.phi, rtol=rtol)
        assert self.grid == other.grid

        data_space = self.data_space + other.data_space
        return SphereSurfModel(data_space, self.phi, self.theta, self.grid)

    def __sub__(self, other, rtol=1e-10):
        np.testing.assert_allclose(self.theta, other.theta, rtol=rtol)
        np.testing.assert_allclose(self.phi, other.phi, rtol=rtol)
        assert self.grid == other.grid

        data_space = self.data_space - other.data_space
        return SphereSurfModel(data_space, self.phi, self.theta, self.grid)

    def copy(self):
        return self.__class__(
            self.data_space.copy(),
            self.phi.copy(),
            self.theta.copy(),
            self.grid,
        )

    @staticmethod
    def cell_centered_grid_phi(nphi):
        phi = np.linspace(0, 2 * np.pi, nphi, endpoint=False)
        phi += phi[1] / 2.0
        return phi

    @staticmethod
    def cell_centered_grid_theta(ntheta):
        theta = np.linspace(0, np.pi, ntheta, endpoint=False)
        theta += theta[1] / 2.0
        return theta

    @classmethod
    def ellipsoid(
        self,
        flattening="GRS80",
        radius=6371e3,
        nphi=100,
        ntheta=200,
        phi=None,
        theta=None,
        grid="None",
    ):

        if type(flattening) is str:
            f = ELLIPSOIDS[flattening]
        else:
            f = float(flattening)

        b = (1.0 - f) ** (2.0 / 3.0) * radius
        e = (2 * f - f ** 2) ** 0.5

        if theta is None and phi is None and grid is None:
            grid = "regular"

        if phi is None:
            phi = self.cell_centered_grid_theta(nphi)

        if theta is None:
            theta = self.cell_centered_grid_theta(ntheta)

        tm, pm = np.meshgrid(theta, phi)
        r = b / (1.0 - e ** 2 * np.sin(tm) ** 2) ** 0.5

        return self(r - radius, phi, theta, grid=grid)

    @classmethod
    def read_crustal_thickness_wieczorek(self, fname, index=4):
        # guess shape assuming the samping includes the pole, hence
        #   N = (n + 1) * 2 * n
        # read crustal thickness models in the xyz format by Marc Wieczorek
        data = np.loadtxt(fname)[:, index]
        N = data.shape[0]
        n = int(-0.5 + (0.25 + N / 2.0) ** 0.5)
        shape = (n + 1, 2 * n)
        data = data.reshape(shape).T

        # interpolate to latitude grid that does not include the poles (as
        # needed by shtns)
        # TODO: do this with a spline interpolation, or even better with
        # spherical harmonics including the poles: sht_reg_poles
        data = (data[:, :-1] + data[:, 1:]) / 2

        return self(data, grid="regular")

    @classmethod
    def read_crustal_thickness_wieczorek_dat(self, fname):
        # guess shape assuming the samping includes the pole, hence
        #   N = (n + 1) * (2 * n + 1)
        # read crustal thickness models in the xyz format by Marc Wieczorek
        data = np.loadtxt(fname)
        N = data.shape[0]
        n = int(((8 * N + 1) ** 0.5 - 3.0) / 4.0)
        shape = (n + 1, 2 * n + 1)
        data = data.reshape(shape).T

        # interpolate to latitude grid that does not include the poles (as
        # needed by shtns)
        # TODO: do this with a spline interpolation, or even better with
        # spherical harmonics including the poles: sht_reg_poles
        data = (data[:, :-1] + data[:, 1:]) / 2
        data = data[:-1, :]

        # to SI unit
        data *= 1e3

        return self(data, grid="regular")

    @classmethod
    def read_Earth2014_grid(self, fname):
        # guess shape assuming the sampling does not include the pole, hence
        #   N = 2 * n ** 2

        data = np.fromfile(fname, dtype=">i2")
        N = data.shape[0]
        n = int((N / 2.0) ** 0.5)
        shape = (n, 2 * n)
        data = data.reshape(shape).T

        data = data[:, ::-1]
        data = data.astype("float")

        nphi, ntheta = data.shape
        phi = self.cell_centered_grid_phi(nphi)
        theta = self.cell_centered_grid_theta(ntheta)

        # Earth 2014 uses geocentric latitude, convert it to geocentric
        theta = self.geographic_to_geocentric(theta, "GRS80")

        return self(data, phi=phi, theta=theta, grid="None")

    @classmethod
    def read_Earth2014_shc(
        self, fname, lmax=128, nphi=None, ntheta=None, grid="regular"
    ):

        nphi = nphi or lmax * 2 + 2
        ntheta = ntheta or lmax + 2

        # Double precission,
        data = np.fromfile(fname, dtype=np.float64)
        # First two values denote the available spherical harmonics degrees.
        lmin_file = int(data[0])
        lmax_file = int(data[1])
        data = data[2:]

        # Read all coefficients.
        nl = lmax_file - lmin_file + 1
        ncoeff = nl * (nl + 1) // 2
        # Assert the computation is consistent with the file size.
        assert ncoeff == data.shape[0] / 2
        Cnm = data[:ncoeff]
        Snm = data[ncoeff : 2 * ncoeff]  # NOQA

        sh = sht(min(lmax, lmax_file), nphi, ntheta, grid=grid)
        ylm = sh.sh.spec_array()
        idx = np.arange(sh.sh.nlm)[np.lexsort([sh.sh.m, sh.sh.l])]
        ylm[idx] = Cnm[: len(idx)] - 1j * Snm[: len(idx)]
        ylm[sh.sh.m > 0] /= 2 ** 0.5

        data = sh.isht(ylm)
        return self(data, sh.phi, sh.theta, grid=grid)

    @classmethod
    def read_crust1(self, fname, index=(1, -1)):
        data = np.loadtxt(fname)
        if type(index) is str:
            index = CRUST_1_0_LAYER[index]

        if type(index) is tuple:
            data = data[:, index[0]] - data[:, index[1]]
        elif type(index) is int:
            data = data[:, index]
        else:
            raise ValueError("index should be tuple, int or string")

        N = data.shape[0]
        n = int((N / 2.0) ** 0.5)
        shape = (n, 2 * n)
        data = data.reshape(shape).T

        # to SI units
        data *= 1e3

        nphi, ntheta = data.shape
        phi = self.cell_centered_grid_phi(nphi)
        theta = self.cell_centered_grid_theta(ntheta)

        # crust 1.0 uses geographic = geodetic latitude, convert it to
        # geocentric
        theta = self.geographic_to_geocentric(theta, "GRS80")

        return self(data, phi=phi, theta=theta, grid="None")

    @classmethod
    def read_EGM2008_undulations(self, fname):

        data = np.fromfile(fname, dtype="float32")
        N = data.shape[0]
        n = int((N / 2.0) ** 0.5)
        nphi, ntheta = (2 * n, n)
        shape = (ntheta, nphi)
        data = data.reshape(shape).T

        # not sure why, but it seems the file contains extra zeros, after
        # removing them, the shape and statistical properties match the ones
        # quoted in the readme
        # http://earth-info.nga.mil/GandG/wgs84/gravitymod/egm2008/README_WGS84_2.pdf  # NoQa
        data = data[1:-1]
        nphi -= 2

        theta = np.linspace(0.0, np.pi, ntheta, endpoint=True)
        theta = self.geographic_to_geocentric(theta, "WGS84")
        phi = np.linspace(0.0, 2 * np.pi, nphi, endpoint=False)
        ssm = self(data, theta=theta, phi=phi, grid="None")

        ssm.rotate_regdata_180()
        return ssm

    @classmethod
    def read_mola_img(self, fname):

        # data is msb integer, convert to python standard lsb
        if type(fname) is str:
            data = np.fromfile(fname, dtype=np.int16).byteswap()
            N = data.shape[0]
            n = int((N / 2.0) ** 0.5)
            nphi, ntheta = (2 * n, n)
            shape = (ntheta, nphi)
            data = data.reshape(shape).T

        elif type(fname) is list:
            data = []
            for fn in fname:
                data.append(np.fromfile(fn, dtype=np.int16).byteswap())

            N = data[0].shape[0]
            n = int((N / 2.0) ** 0.5)
            nphi, ntheta = (2 * n, n)
            shape = (ntheta, nphi)
            for i in range(len(data)):
                data[i] = data[i].reshape(shape).T
            d1 = np.concatenate((data[0], data[1]), axis=0)
            d2 = np.concatenate((data[2], data[3]), axis=0)
            data = np.concatenate((d1, d2), axis=1)
        else:
            raise TypeError()

        data = data.astype("float")

        ssm = self(data, grid="regular")
        ssm.rotate_regdata_180()
        return ssm

    @classmethod
    def read_tpx08(self, fname, var="hRe"):
        nc = Dataset(fname, "r")

        # we need theta increasing
        phi = np.deg2rad(nc.variables["lon_z"][:])
        theta = np.deg2rad(90.0 - nc.variables["lat_z"][::-1])
        theta = np.clip(theta, 0.0, np.pi)
        theta = self.geographic_to_geocentric(theta, "WGS84")
        data = nc.variables[var][:, ::-1]

        data = data.astype("float")

        # elevation is in mm -> to SI
        data /= 1e3

        # set data at the poles to mean
        data[:, 0] = data[:, 0].mean()
        data[:, -1] = data[:, -1].mean()

        ssm = self(data, phi=phi, theta=theta)
        ssm.rotate_regdata_180()
        return ssm

    @staticmethod
    def geographic_to_geocentric(theta, flattening="GRS80"):

        if type(flattening) is str:
            f = ELLIPSOIDS[flattening]
        else:
            f = float(flattening)

        e2 = 2 * f - f ** 2
        return np.pi / 2.0 - np.arctan((1 - e2) * np.tan(np.pi / 2.0 - theta))

    @property
    def shape(self):
        return self.data_space.shape

    @property
    def nphi(self):
        return self.data_space.shape[0]

    @property
    def ntheta(self):
        return self.data_space.shape[1]

    @property
    def max_lmax(self):
        nphi, ntheta = self.shape
        lmax = min(ntheta - 2, nphi / 2 - 1)
        return lmax

    def downsample(self, factor=2):
        data = self.data_space.reshape(
            (self.nphi / factor, factor, self.ntheta / factor, factor)
        )

        self.data_space = data.mean(axis=(1, 3))
        self.phi = self.phi.reshape((-1, factor)).mean(axis=1)
        self.theta = self.theta.reshape((-1, factor)).mean(axis=1)

    def rotate_ylm(self, euler_angles=(None, None, None), lmax_transform=None):
        lmax_transform = lmax_transform or self.max_lmax
        npts_ph, npts_th = self.shape

        sh = sht(lmax_transform, npts_ph, npts_th, grid=self.grid)
        ylm = sh.sht(self.data_space, theta=self.theta, phi=self.phi)
        ylm = sh.rotate_ylm(ylm, euler_angles, phi_0=self.phi[0])
        self.data_space = sh.isht(ylm)

    def rotate_regdata_180(self):
        data = np.zeros_like(self.data_space)
        nphi = self.shape[0]
        data[: nphi // 2] = self.data_space[nphi // 2 :]  # NOQA
        data[nphi // 2 :] = self.data_space[: nphi // 2]  # NOQA

        self.data_space = data

    def write_ascii(self, fname, **kwargs):
        np.savetxt(fname, self.data_space, **kwargs)
        np.savetxt(fname + ".theta", self.theta, **kwargs)
        np.savetxt(fname + ".phi", self.phi, **kwargs)

    def write_cf(self, fname: pathlib.Path, vname: str, cf_kwargs=None):
        """
        Write the current spherical model to a NetCDF file. If the file
        exists the variable will be appended to it, otherwise a new file will
        be created.

        Args:
            fname: Filename.
            vname: Name of the variable in the file.
        """
        latitude = 90.0 - np.rad2deg(self.theta)
        longitude = np.rad2deg(self.phi)

        assert longitude.shape > latitude.shape

        latitude_dim_name = "latitude_%s" % vname
        longitude_dim_name = "longitude_%s" % vname

        ds = xr.Dataset(
            data_vars={
                vname: (
                    [longitude_dim_name, latitude_dim_name],
                    np.require(self.data_space, dtype=np.float32),
                )
            },
            coords={
                longitude_dim_name: np.require(longitude, dtype=np.float32),
                latitude_dim_name: np.require(latitude, dtype=np.float32),
            },
        )

        # Units and names for the dimension axes.
        getattr(ds, latitude_dim_name).attrs["units"] = "degrees_north"
        getattr(ds, longitude_dim_name).attrs["units"] = "degrees_east"
        getattr(ds, latitude_dim_name).attrs[
            "standard_name"
        ] = latitude_dim_name
        getattr(ds, longitude_dim_name).attrs[
            "standard_name"
        ] = longitude_dim_name

        # Also for the actual data.
        getattr(ds, vname).attrs["units"] = "m"
        getattr(ds, vname).attrs["standard_name"] = vname

        # ds.attrs["geospatial_lat_units"] = "degrees_north"
        # ds.attrs["geospatial_lon_units"] = "degrees_east"
        ds.attrs["Conventions"] = "CF-1.5"

        if fname.exists():
            mode = "a"
        else:
            mode = "w"

        encoding = {
            vname: {
                "zlib": True,
                "complevel": 9,
                "chunksizes": (
                    min(self.data_space.shape[0], 1024),
                    min(self.data_space.shape[1], 1024),
                ),
            }
        }

        # The engine must be `netcdf4`, otherwise Paraview cannot correctly
        # open the files.
        ds.to_netcdf(fname, mode=mode, engine="netcdf4", encoding=encoding)

    def spline_interp_new_grid(
        self,
        nphi=200,
        ntheta=100,
        phi=None,
        theta=None,
        # sphere=True,
        order=None,
        grid=None,
        epsilon=1e-10,
        abs_gradient=False,
    ):
        # Only allow spherical interpolation - the other option is faster but
        # more error prone.
        if True:
            spl = RectSphereBivariateSpline(
                np.clip(self.theta, epsilon, np.pi - epsilon),
                self.phi,
                self.data_space.T,
            )
        else:
            #
            spl = RectBivariateSpline(
                self.theta, self.phi, self.data_space.T, kx=order, ky=order
            )

        if theta is None and phi is None:
            grid = "regular"

        if phi is None:
            phi = self.cell_centered_grid_phi(nphi)

        if theta is None:
            theta = self.cell_centered_grid_theta(ntheta)

        if not abs_gradient:
            self.data_space = spl(theta, phi).T
        else:
            dt = spl(theta, phi, dtheta=1).T
            dp = spl(theta, phi, dphi=1).T
            self.data_space = (dt ** 2 + (dp / np.sin(theta)) ** 2) ** 0.5

        self.theta = theta.copy()
        self.phi = phi.copy()
        self.grid = grid or self.grid

    def filter_model(
        self, lmax_filter, lmax_transform=None, order=2, filter_type="gauss"
    ):

        lmax_transform = lmax_transform or self.max_lmax
        npts_ph, npts_th = self.shape

        sh = sht(lmax_transform, npts_ph, npts_th, grid=self.grid)
        ylm = sh.sht(self.data_space)

        if filter_type == "gauss":
            # Al-Attar (2014), eq E3, gauss
            ylm *= np.exp(
                -2
                * np.pi
                * (sh.l + 0.0) ** order
                / (2 * lmax_filter + 0.0) ** order
            )
        elif filter_type == "butterworth":
            ylm *= 1.0 / (1.0 + (sh.l * 1.0 / lmax_filter) ** (2 * order))
        elif filter_type == "cutoff":
            ylm[sh.l > lmax_filter] = 0.0
        else:
            raise ValueError()

        self.data_space = sh.isht(ylm)

    def compute_spectral_power(self, lmax=None):

        lmax = lmax or self.max_lmax
        npts_ph, npts_th = self.shape
        sh = sht(lmax, npts_ph, npts_th, grid=self.grid)

        ylm = sh.sht(self.data_space)

        power = np.zeros(lmax + 1)
        for l in np.arange(lmax + 1):
            sl = sh.l == l  # NOQA
            power[l] = 1.0 / (2 * l + 1) * (np.abs(ylm[sl] ** 2)).sum()

        return power

    def plot_spectral_power(self, lmax=None, fig=None, show=True):
        if fig is None:
            fig = plt.figure()
        ax = fig.gca()

        power = self.compute_spectral_power(lmax)
        ax.loglog(power)

        if show:
            plt.show()
        else:
            return fig

    def plot(self, cmap=None, vmin=None, vmax=None, show=True, cblabel=None):
        fig = plt.figure()
        ax = plt.gca()
        ax.set_aspect("equal")

        im = NonUniformImage(ax, extent=[0.0, 360.0, -90.0, 90.0], cmap=cmap)
        im.set_data(
            np.rad2deg(self.phi) - 180.0,
            np.rad2deg(self.theta) - 90,
            self.data_space.T[::-1].copy(),
        )
        ax.images.append(im)

        plt.xlim(-180.0, 180.0)
        plt.ylim(-90.0, 90.0)
        if vmin or vmax is not None:
            im.set_clim(vmin, vmax)
        ax.set_xticks([-180.0, -90.0, 0.0, 90.0, 180.0])
        ax.set_yticks([-90.0, -45.0, 0.0, 45, 90.0])
        plt.xlabel("longitude")
        plt.ylabel("latitude")

        # add colorbar
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.2)
        plt.colorbar(im, cax=cax, label=cblabel)

        if show:
            plt.show()
        else:
            return fig
