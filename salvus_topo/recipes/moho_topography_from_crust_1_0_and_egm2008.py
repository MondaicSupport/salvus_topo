"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import typing

import click

from ..remote_data import fetch
from ..sht import sht
from ..sphere_surf_model import SphereSurfModel

max_lmax = 256
lmax_degrees = [16, 32, 64, 128, 256]


def get_spherical_model() -> typing.Tuple[
    SphereSurfModel, typing.Dict[str, typing.Any]
]:
    grid = "gauss"

    path_crust = fetch("CRUST_1.0/crust1.0.tar.gz")["crust1.bnds"]
    path_geoid = fetch(
        "EGM2008/Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz"
    )["Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz.decomp"]

    # Add the geoid, treated separately as it is sampled differently
    click.echo("Reading geoid data from file ...")
    geoid = SphereSurfModel.read_EGM2008_undulations(path_geoid)

    # Generate the theta grid
    sh = sht(4, geoid.nphi, geoid.ntheta + 1, grid=grid)

    # Interpolate to gauss grid with same number of samples to enable sht as
    # this is totally oversampled, use nonspherical, linear interpolation for
    # speed
    click.echo("Interpolating geoid ...")
    geoid.spline_interp_new_grid(
        phi=sh.phi, theta=sh.theta, grid=grid, order=1
    )

    click.echo("Reading crustal thickness, ocean, and topo from CRUST 1.0 ...")
    crustal_thickness = SphereSurfModel.read_crust1(path_crust, index="crust")
    ocean = SphereSurfModel.read_crust1(path_crust, index="ocean")
    topo = SphereSurfModel.read_crust1(path_crust, index="topography")

    moho_shape = topo - ocean - crustal_thickness

    # Interpolate to the geoid grid (upsampling)
    click.echo("Interpolating Moho ...")
    moho_shape.spline_interp_new_grid(
        phi=geoid.phi, theta=geoid.theta, grid=geoid.grid
    )

    # Relative to wgs84 Geoid
    print("Adding models ...")
    moho_shape = moho_shape + geoid

    return moho_shape
