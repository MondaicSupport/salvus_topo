"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing

import click

from ..remote_data import fetch
from ..sht import sht
from ..sphere_surf_model import SphereSurfModel

grid = "gauss"
max_lmax = 10800

lmaxmax_geoid = 2160
lmaxmax_topo = 10800


def custom(output_filename: pathlib.Path, lmax_degrees: typing.List[int]):
    click.echo("Checking Earth 2014 TBI (Topo, Bedrock, Ice) file ...")
    path_topo = fetch("EARTH_2014/Earth2014.TBI2014.degree10800.bshc")[
        "Earth2014.TBI2014.degree10800.bshc"
    ]

    path_geoid = fetch(
        "EGM2008/Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz"
    )["Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz.decomp"]

    # Add the geoid, treated separately as it is sampled differently
    click.echo("Reading Geoid ...")
    geoid = SphereSurfModel.read_EGM2008_undulations(path_geoid)

    # Generate the theta grid
    sh = sht(4, geoid.nphi, geoid.ntheta + 1, grid=grid)

    # Interpolate to gauss grid with same number of samples to enable sht.
    click.echo("Interpolating Geoid ...")
    geoid.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid)

    for _i, lmax in enumerate(lmax_degrees):
        click.echo(
            f"Computing model for l_max {lmax} ({_i + 1} of "
            f"{len(lmax_degrees)})"
        )
        lmax_transform = min(lmax * 2, lmaxmax_topo)

        nphi = lmax * 4 + 4
        ntheta = lmax * 2 + 4

        click.echo("    -> Reading TBI ...")
        topo = SphereSurfModel.read_Earth2014_shc(
            path_topo, lmax_transform, grid=grid, nphi=nphi, ntheta=ntheta
        )

        if lmax < lmaxmax_topo:
            click.echo("    -> Spectral filtering topography ...")
            topo.filter_model(lmax, lmax_transform)

        geoid_l = geoid.copy()
        if lmax < lmaxmax_geoid:
            click.echo("    -> Spectral filtering geoid ...")
            geoid_l.filter_model(lmax, lmax_transform)

        click.echo("    -> Interpolating geoid ...")
        geoid_l.spline_interp_new_grid(
            phi=topo.phi, theta=topo.theta, grid=grid
        )

        topo_wgs84 = geoid_l + topo

        topo_wgs84.rotate_regdata_180()

        click.echo("    -> Writing to file ...")
        topo_wgs84.write_cf(
            output_filename, vname=f"{output_filename.stem}_lmax_{lmax}"
        )
