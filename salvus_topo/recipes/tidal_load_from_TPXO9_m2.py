"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing
import numpy as np

from astropy.convolution import convolve, TrapezoidDisk2DKernel
from netCDF4 import Dataset


import click

from ..remote_data import fetch
from ..sphere_surf_model import SphereSurfModel
from ..sht import sht

grid = "gauss"
max_lmax = 10800
lmaxmax = 10800

cf_kwargs = {
    "fmt": "NETCDF4",
    "compress": 2,
    "mode": "w",
    "single": True,
    "HDF_chunksizes": {0: 1024, 1: 1024},
}


def extrapolate(data_in, load_mask):
    kernel = np.ones((3, 3))

    # correct for wrap from north to south pole, luckily no water at south pole
    smoothed = convolve(data_in, kernel, boundary='wrap', mask=load_mask)
    smoothed[:, -1] = np.nan
    smoothed = convolve(smoothed, kernel, boundary='wrap')
    smoothed[:, -1] = np.nan

    coast_mask = load_mask * np.logical_not(np.isnan(smoothed))
    data = data_in.copy()
    data[load_mask] = np.nan
    data[coast_mask] = smoothed[coast_mask]

    return data


def upsample(data_in, load_mask=None):
    # upsample using the astropy convolve function to deal with the
    # discontinuity using NaNs
    data_in = data_in.copy()
    if load_mask:
        data_in[load_mask] = np.nan

    nphi, ntheta = data_in.shape
    nphi = nphi * 2
    ntheta = ntheta * 2 - 1

    data = np.zeros((nphi, ntheta)) * np.nan
    data[1::2, ::2] = data_in[:, :]

    kernel = np.ones((3, 3))
    data = convolve(data, kernel, boundary='wrap')

    # correct for wrap from north to south pole, luckily no water at south pole
    data[:, -1] = np.nan

    return data


def custom(output_filename: pathlib.Path, lmax_degrees: typing.List[int],
           tide: str = 'm2'):

    assert tide in ['m2', 'o1', 'mf']
    click.echo("Checking TPXO9 file ...")
    path = fetch(f"TPXO9/h_{tide}_tpxo9_atlas_30_v3.nc")[
        f"h_{tide}_tpxo9_atlas_30_v3.nc"
    ]

    click.echo("Reading TPXO9 ...")
    re = SphereSurfModel.read_tpx08(path, var="hRe")
    im = SphereSurfModel.read_tpx08(path, var="hIm")

    # clean up funny values (like near melbourne in O1). Set all values with
    # absolute value > 10 to 0.
    re.data_space[np.abs(re.data_space) > 10] = 0.
    im.data_space[np.abs(im.data_space) > 10] = 0.

    click.echo("extrapolate and upsample (NaN warning expected!) ...")
    load_mask = (re.data_space == 0) * (im.data_space == 0)

    re_data = extrapolate(re.data_space, load_mask)
    re_data = upsample(re_data)
    re.data_space = re_data
    re.theta = np.linspace(0., np.pi, re_data.shape[1])
    re.phi = np.linspace(0., 2 * np.pi, re_data.shape[0]+1)[1:]

    im_data = extrapolate(im.data_space, load_mask)
    im_data = upsample(im_data)
    im.data_space = im_data
    im.theta = np.linspace(0., np.pi, im_data.shape[1])
    im.phi = np.linspace(0., 2 * np.pi, im_data.shape[0]+1)[1:]

    click.echo("get land / sea mask and convert to weight ...")
    path = fetch(f"GSSHG/land_mask.nc")["land_mask.nc" ]
    d = Dataset(path, 'r')
    sea = d['z'][:, :][::-1, 1:].T

    # this convolution should be equivalent to a projection on triangular functions
    kernel = TrapezoidDisk2DKernel(0, slope=0.5)
    sea_weight = convolve(sea, kernel, boundary='wrap')
    sea_weight = sea_weight[1::2, ::2]

    # correct poles for wrap around
    sea_weight[:, 0] = 1   # northpole is wet
    sea_weight[:, -1] = 0  # southpole is dry

    # finally compute the loads
    re.data_space = np.nan_to_num(re.data_space * sea_weight)
    im.data_space = np.nan_to_num(im.data_space * sea_weight)

    # use the sea weight for refinement
    sea_mask = re.copy()
    sea_mask.data_space = sea_weight

    click.echo("interpolate to gauss grid")
    # just generate the theta grid
    sh = sht(4, re.nphi + 2, re.ntheta + 1, grid=grid)

    re.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid, order=1)
    im.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid, order=1)
    sea_mask.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid,
                                    order=1)

    for _i, lmax in enumerate(lmax_degrees):
        click.echo(
            f"Computing model for l_max {lmax} ({_i + 1} of "
            f"{len(lmax_degrees)})"
        )

        lmax_transform = min(lmax * 2, lmaxmax)

        re_l = re.copy()
        im_l = im.copy()
        sea_mask_l = sea_mask.copy()

        nphi = lmax * 4 + 4
        ntheta = lmax * 2 + 4

        if lmax < lmaxmax:
            click.echo("    -> Spectral filtering ...")
            re_l.filter_model(lmax, lmax_transform)
            im_l.filter_model(lmax, lmax_transform)
            sea_mask_l.filter_model(lmax, lmax_transform)

        click.echo("    -> Interpolate to regular grid ...")
        re_l.spline_interp_new_grid(nphi=nphi, ntheta=ntheta, grid="regular")
        im_l.spline_interp_new_grid(nphi=nphi, ntheta=ntheta, grid="regular")

        click.echo("    -> Compute amplitude ...")
        amp = re_l.copy()
        amp.data_space = (re_l.data_space ** 2 + im_l.data_space ** 2) ** 0.5

        click.echo("    -> Compute gradient ...")
        grad = amp.copy()
        grad.spline_interp_new_grid(
            nphi=nphi, ntheta=ntheta, grid="regular", abs_gradient=True
        )

        click.echo("    -> Compute gradient of sea mask...")
        sea_mask_l.spline_interp_new_grid(
            nphi=nphi, ntheta=ntheta, grid="regular", abs_gradient=True
        )

        click.echo("    -> Rotating ...")
        re_l.rotate_regdata_180()
        im_l.rotate_regdata_180()
        amp.rotate_regdata_180()
        grad.rotate_regdata_180()
        sea_mask_l.rotate_regdata_180()

        click.echo("    -> Writing to file ...")
        re_l.write_cf(
            output_filename,
            vname="tidal_elevation_re_lmax_%d" % lmax,
            cf_kwargs=cf_kwargs,
        )
        cf_kwargs["mode"] = "a"
        im_l.write_cf(
            output_filename,
            vname="tidal_elevation_im_lmax_%d" % lmax,
            cf_kwargs=cf_kwargs,
        )
        amp.write_cf(
            output_filename,
            vname="tidal_elevation_amp_lmax_%d" % lmax,
            cf_kwargs=cf_kwargs,
        )
        grad.write_cf(
            output_filename,
            vname="tidal_elevation_grad_lmax_%d" % lmax,
            cf_kwargs=cf_kwargs,
        )
        sea_mask_l.write_cf(
            output_filename,
            vname="tidal_elevation_sea_mask_grad_lmax_%d" % lmax,
            cf_kwargs=cf_kwargs,
        )
