"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import os
import inspect
import numpy as np
import matplotlib.pyplot as plt
from sht import sht
import scipy.misc

from sphere_surf_model import SphereSurfModel

DATA_DIR = os.path.join(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
    "data",
)

grid = "gauss"
fname_topo = "meg016_megr90n000cb.img"

# fname_topo = []
# fname_topo.append('meg064_megr90n000cb.img')
# fname_topo.append('meg064_megr90n180cb.img')
# fname_topo.append('meg064_megr00n000cb.img')
# fname_topo.append('meg064_megr00n180cb.img')


cf_kwargs = {
    "fmt": "NETCDF4",
    "compress": 2,
    "mode": "w",
    "single": True,
    "HDF_chunksizes": {0: 1024, 1: 1024},
}

llmax = [1024]  # [16, 32, 64, 128, 256, 512, 1024]
lmaxmax_topo = 2878

fname_out = "mars_topography_filtered"
plot = True
show = False
pdf = False
jpg = True
fancy = True

if type(fname_topo) is list:
    path_topo = [os.path.join(DATA_DIR, fn) for fn in fname_topo]
else:
    path_topo = os.path.join(DATA_DIR, fname_topo)

topo = SphereSurfModel.read_mola_img(path_topo)

# just generate the theta grid
sh = sht(4, topo.nphi, topo.ntheta, grid=grid)

# interpolate to gauss grid with same number of samples to enable sht
print("interp topo")
topo.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid)

for lmax in llmax:
    print(lmax)
    lmax_transform = min(lmax * 2, lmaxmax_topo)

    nphi = lmax * 4 + 4
    ntheta = lmax * 2 + 4

    topo_l = topo.copy()
    if lmax < lmaxmax_topo:
        print("filter topo")
        topo_l.filter_model(lmax, lmax_transform)

    print("ds topo")
    phi = np.linspace(0, 2 * np.pi, nphi, endpoint=False)
    theta = np.linspace(0, np.pi, ntheta, endpoint=False)
    theta += theta[1] / 2.0
    topo_l.spline_interp_new_grid(phi=phi, theta=theta, grid="regular")

    print("reference to ellipsoid")
    elli = SphereSurfModel.ellipsoid(
        "MARS", radius=3389500.0, phi=phi, theta=theta, grid="regular"
    )

    topo_l.data_space = topo_l.data_space + 3396000.0 - 3389500.0
    topo_l = topo_l - elli

    if plot:
        print("plotting")
        if pdf or show:
            topo_l.plot(show=False, cblabel="topo_lgraphy relative to WGS84")
            plt.savefig(fname_out + "_lmax_%d.pdf" % lmax)

        if jpg:
            scipy.misc.imsave(
                fname_out + "_lmax_%d.jpg" % lmax, topo_l.data_space.T
            )

        if fancy:
            ss = np.argsort(topo_l.data_space.ravel())
            dn = np.zeros_like(ss)
            dn[ss] = np.arange(ss.size)
            dn = dn.reshape(topo_l.shape)
            scipy.misc.imsave("dnt_mars.png", dn.T)

    # rotate, because in salvus_mesher we need to start from longitude 0.
    topo_l.rotate_regdata_180()

    print("write")
    topo_l.write_cf(
        fname_out + ".nc",
        vname=fname_out + "_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )
    cf_kwargs["mode"] = "a"

plt.show()
