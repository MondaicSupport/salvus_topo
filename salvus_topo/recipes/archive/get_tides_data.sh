#!/bin/bash

mkdir tides_data
cd tides_data

# TPX08 atlas elevation data
# wget ftp://ftp.oce.orst.edu/dist/tides/TPXO8_atlas_30_v1_nc/hf.m2_tpxo8_atlas_30c_v1.nc

# TPX09 atlas elevation data
wget ftp://ftp.oce.orst.edu/dist/tides/TPXO9_atlas_nc/h_m2_tpxo9_atlas_30.nc

cd ..
