"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing

import click

from ..remote_data import fetch
from ..sphere_surf_model import SphereSurfModel
from ..sht import sht
from .tidal_load_from_TPXO9_m2 import custom as custom_m2


max_lmax = 10800


def custom(output_filename: pathlib.Path, lmax_degrees: typing.List[int]):
    custom_m2(output_filename, lmax_degrees, 'o1')
