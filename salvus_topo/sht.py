"""
A wrapper around the shtns spherical harmonics transform meant to pythonify
the interface

(c) Mondaic AG (info@mondaic.com), 2020
"""
import numpy as np
import shtns


class sht(object):
    """
    A wrapper around the shtns spherical harmonics transform meant to pythonify
    the interface
    """

    def __init__(
        self,
        lmax,
        nphi,
        ntheta,
        grid="regular",
        polar_opt_threshold=1e-10,
        load_save_cfg=True,
        quick_init=True,
    ):

        self.sh = shtns.sht(lmax, lmax, norm=shtns.sht_fourpi)

        if grid == "regular":
            grid_typ = shtns.sht_reg_fast
        elif grid == "gauss":
            grid_typ = shtns.sht_gauss
        else:
            raise ValueError("unkown grid for sht: %s" % (grid,))

        grid_typ = grid_typ | shtns.SHT_THETA_CONTIGUOUS
        if load_save_cfg:
            grid_typ = grid_typ | shtns.SHT_LOAD_SAVE_CFG

        if ntheta >= 32 and grid == "gauss" and quick_init:
            grid_typ = grid_typ | shtns.sht_quick_init

        self.sh.set_grid(
            ntheta, nphi, flags=grid_typ, polar_opt=polar_opt_threshold
        )

    def sht(self, data, theta=None, phi=None, rtol=1e-7):
        """
        if phi or theta are passed, they are verified against the expected grid
        of the spherical harmonic transform
        """
        if theta is not None:
            np.testing.assert_allclose(self.theta, theta, rtol=rtol)
        if phi is not None:
            np.testing.assert_allclose(self.phi, phi - phi[0], rtol=rtol)
        return self.sh.analys(data)

    def isht(self, ylm):
        return self.sh.synth(ylm)

    def rotate_ylm(self, ylm, euler_angles=(None, None, None), phi_0=None):
        # correct for cell registered sampling in phi direction
        if phi_0:
            euler_angles = list(euler_angles)
            euler_angles[0] = (
                euler_angles[0] - phi_0 if euler_angles[0] else -phi_0
            )
            euler_angles[2] = (
                euler_angles[2] + phi_0 if euler_angles[2] else phi_0
            )

        if euler_angles[0] is not None:
            ylm = shtns.sht.Zrotate(self.sh, ylm, np.deg2rad(euler_angles[0]))

        if euler_angles[1] is not None:
            ylm = shtns.sht.Yrotate(self.sh, ylm, np.deg2rad(euler_angles[1]))

        if euler_angles[2] is not None:
            ylm = shtns.sht.Zrotate(self.sh, ylm, np.deg2rad(euler_angles[2]))

        return ylm

    @property
    def l(self):  # NOQA
        return self.sh.l

    @property
    def nphi(self):
        return self.sh.nphi

    @property
    def ntheta(self):
        return self.sh.nlat

    @property
    def theta(self):
        return np.arccos(self.sh.cos_theta)

    @property
    def phi(self):
        return np.linspace(0, 2 * np.pi, self.nphi, endpoint=False)
